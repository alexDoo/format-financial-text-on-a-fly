// user: d3eca178-ae0f-4a42-bdb3-5275dd5ec4b3

/* helper functions */
function placeCaretAtEnd(el) {
    el.focus();
    if (typeof window.getSelection != "undefined"
            && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
    }
}
// retrieve pre saved content from server
function make_ini() {
    $.get('https://evening-savannah-67907.herokuapp.com/api/v1/sys/signals/d3eca178-ae0f-4a42-bdb3-5275dd5ec4b3',
        function(resp, textStatus, xhr){
            if (xhr.status === 200 && typeof resp.detail === 'undefined') {
                $('#editable').html(text_to_html(resp.content));
                placeCaretAtEnd(document.getElementById("editable"));
            } else {
                setTimeout(function(){
                    make_ini();
                }, 400);
            }
        }
    );
}
// save new content
function make_save(content) {
    $.ajax({
        url: 'https://evening-savannah-67907.herokuapp.com/api/v1/sys/signals',
        type: 'PUT',
        dataType: 'json',
        data: { user: 'd3eca178-ae0f-4a42-bdb3-5275dd5ec4b3', content: content },
        success: function(resp) {
           if (typeof resp.detail !== 'undefined') {
               setTimeout(function(){
                   console.log('resave');
                   make_save(content);
               }, 400);
           }
       },
       error: function(){
           setTimeout(function(){
               console.log('resave');
               make_save(content);
           }, 400);
       }
    });
}
/* end helper functions */

/* this is our babe */
function text_to_html(text) {
    text = text.replace(/([^a-z]and[^a-z]|[^a-z]or[^a-z])/g,' <span class="tblue">' +'$1'.toUpperCase()+ '</span> ');
    text = text.replace(/([^a-z]and|[^a-z]or)$/g,' <span class="tblue">' +'$1'.toUpperCase()+ '</span> ');
    text = text.replace(/([^a-z]lower[^a-z]|[^a-z]lower than[^a-z]|[^a-z]higher than[^a-z])/g, ' <span class="tbold">$1</span> ');
    text = text.replace(/([^a-z]lower|[^a-z]lower than|[^a-z]higher than)$/g, ' <span class="tbold">$1</span> ');
    //exponential moving average
    text = text.replace(/([^a-z]exponential moving average)/g, ' <span class="tbold tgreen">$1</span> ');
    //moving average | simple moving average
    text = text.replace(/([^a-z]moving average |[^a-z]simple moving average )/g, ' <span class="tbold torange">$1</span> ');
    text = text.replace(/([^a-z]moving average|[^a-z]simple moving average)$/g, ' <span class="tbold torange">$1</span> ');
console.log(text);
    return text;
}

$(function(){
    var editable = $('#editable');

    make_ini();

    editable.on('keyup keydown', function(e){
        if (e.keyCode !== 8 && e.keyCode > 46) {
            editable.html(text_to_html(editable.text()));
            placeCaretAtEnd(document.getElementById("editable"));
        }
    });

    $('#save').on('click', function(){
        make_save($('#editable').text());
    });
});
